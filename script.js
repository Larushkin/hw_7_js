'use strict'
/**
 * Теоретичні питання

1.  forEach перебирає всі елементи масиву і застосовує до них callback функцію.

2.  Існує багато варіантів, але як на мене найкрищий array.length = 0;

3.  Array.isArray - метод який повертає true/false
  
 */


let arr =  ['hello', 'world', 23, '23', null];

function filterBy(array, type){
    return array.filter(element => typeof(element) !== type);
}
console.log(filterBy(arr, 'string'));